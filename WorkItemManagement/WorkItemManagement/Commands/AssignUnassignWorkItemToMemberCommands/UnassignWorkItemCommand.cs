﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Commands.AssignUnassignWorkItem
{
    /// <author>Mihail Popov</author>
    public class UnassignWorkItemCommand : Command
    {
        public UnassignWorkItemCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 5)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "assignee, team, board, work item type and work item title"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[1]);
            var member = team.GetMember(this.CommandParameters[0]);
            
            var board = team.GetBoard(this.CommandParameters[2]);
            var workItemType = this.CommandParameters[3];
            var workItemTitle = this.CommandParameters[4];
            var workItem = board.GetWorkItemByTypeAndTitle(workItemType, workItemTitle);

            member.UnassignItem(workItem as IBugStory);

            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(SuccessfullStrings.UNASSIGNED_WORK_ITEM, workItem.Title, member.Name);
        }
    }
}
