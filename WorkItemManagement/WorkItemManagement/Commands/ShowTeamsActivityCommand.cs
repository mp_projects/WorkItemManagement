﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands
{
    /// <author>Mihail Popov</author>
    public class ShowTeamsActivityCommand : Command
    {
        public ShowTeamsActivityCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);

            StringBuilder teamHistory = new StringBuilder();

            teamHistory.AppendLine($"Team \"{team.Name}\" activity:");
            teamHistory.Append(team.ShowActivity());

            this.Writer.SetPrintColor(StringType.Activity);
            return teamHistory.ToString().Trim();
        }
    }
}
