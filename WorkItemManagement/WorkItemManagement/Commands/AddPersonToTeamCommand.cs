﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Models;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands
{
    /// <author>Georgi Stefanov</author>
    public class AddPersonToTeamCommand : Command
    {
        public AddPersonToTeamCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: addpersontoteam MemberName TeamName

            if (this.CommandParameters.Count != 2)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "member name and team name"));
            }

            // get the team
            string teamName = this.CommandParameters[1];
            var team = this.Database.GetTeam(teamName);

            // get the member
            string memberName = this.CommandParameters[0];
            var member = this.Database.GetMember(memberName);

            team.AddTeammate(member);

            this.Writer.SetPrintColor(StringType.Success);
            return $"Successfully added {memberName} to {teamName}";
        }
    }
}
