﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;

namespace WorkItemManagement.Commands
{
    /// <author>Georgi Stefanov</author>
    public class CreateNewTeamBoardCommand : Command
    {
        public CreateNewTeamBoardCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: createboard BoardName TeamName

            if (this.CommandParameters.Count != 2)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "board name and team name"));
            }

            // get the team object
            string teamName = this.CommandParameters[1];
            var team = this.Database.GetTeam(teamName);

            // get the board object
            string boardName = this.CommandParameters[0];
            team.CheckBoardExistance(boardName);

            var board = this.Factory.CreateBoard(boardName);

            team.AddBoard(board);

            this.Writer.SetPrintColor(StringType.Success);
            return $"Successfully created board {boardName} in team {teamName}.";
        }
    }
}
