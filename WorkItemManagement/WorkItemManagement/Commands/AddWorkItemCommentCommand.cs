﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;

namespace WorkItemManagement.Commands
{
    /// <author>Georgi Stefanov</author>
    class AddWorkItemCommentCommand : Command
    {
        public AddWorkItemCommentCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: addworkitemcomment TeamName BoardName WorkItemType WorkItemTitle Author Comment

            if (this.CommandParameters.Count < 6)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, work item type, work item title, author name, comment text"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);

            var board = team.GetBoard(this.CommandParameters[1]);

            var workItem = board.GetWorkItemByTypeAndTitle(this.CommandParameters[2], this.CommandParameters[3]);

            var author = this.Database.GetMember(this.CommandParameters[4]);

            string commentText = this.CommandParameters[5];
            Comment comment = this.Factory.CreateComment(commentText, author);

            workItem.AddComment(comment);

            comment.Author.EventLog.AddEvent(new Event(
                string.Format(EventStrings.ADD_COMMENT,workItem.GetType().Name.ToLower(),workItem.Title,board,team)));

            board.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_ADD_COMMENT,
                comment.Author.Name,
                workItem.GetType().Name.ToLower(),
                workItem.Title
                )));

            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(
                SuccessfullStrings.COMMENT_ADDED,
                author.Name,
                this.CommandParameters[2].ToLower(),
                workItem.Title,
                board.Name,
                team.Name);
        }
    }
}
