﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands
{
    /// <author>Georgi Stefanov</author>
    class ShowAllTeams : Command
    {
        public ShowAllTeams(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if (this.Database.Teams.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAMS_YET);
            }

            // column widths
            const int col1 = -21;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("List of all teams:");
            //sb.AppendLine($"{"Name",col1}");
            sb.AppendLine(string.Join(
                Environment.NewLine,
                this.Database.Teams.OrderBy(t => t.Name).Select(t => $"{t.Name,col1}").ToList()));
            
            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
