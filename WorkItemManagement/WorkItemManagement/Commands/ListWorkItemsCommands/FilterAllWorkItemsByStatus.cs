﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Commands.ListWorkItemsCommands
{
    /// <author>Mihail Popov</author>
    class FilterAllWorkItemsByStatus : Command
    {
        public FilterAllWorkItemsByStatus(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database, writer)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 2)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "work item type and status"));
            }

            string workItemType = this.CommandParameters[0];
            string statusAsString = this.CommandParameters[1];

            IEnumerable<IWorkItem> filtratedItems = workItemType switch
            {
                "bug" => Enum.TryParse(statusAsString, out BugStatus bugStatus)
                         ? this.Database.GetWorkItemsFilteredBy(bugStatus)
                         : throw new ArgumentException(ErrorStrings.INVALID_BUG_STATUS),

                "story" => Enum.TryParse(statusAsString, out StoryStatus storyStatus)
                           ? this.Database.GetWorkItemsFilteredBy(storyStatus)
                           : throw new ArgumentException(ErrorStrings.INVALID_STORY_STATUS),

                "feedback" => Enum.TryParse(statusAsString, out FeedbackStatus feedbackStatus)
                              ? this.Database.GetWorkItemsFilteredBy(feedbackStatus)
                              : throw new ArgumentException(ErrorStrings.INVALID_FEEDBACK_STATUS),

                _ => throw new ArgumentException(ErrorStrings.INVALID_WORK_ITEM_TYPE)
            };

            string workItemTypeInHeader=workItemType switch
                {
                    "bug" => "bugs",
                    "story" => "stories",
                    "feedback" => "feedbacks",
                    _ => ""
                };

        StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of {workItemTypeInHeader} with status {statusAsString}");
            sb.Append(string.Join(Environment.NewLine, filtratedItems));

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
