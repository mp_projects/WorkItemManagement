﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands.ListWorkItemsCommands
{
    /// <author>Mihail Popov</author>
    public class ListAllWorkItemsCommand : Command
    {
        public ListAllWorkItemsCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if(this.Database.WorkItems.Count==0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "work items"));
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("List of all work items:");
            sb.Append(this.Database.ListAllWorkItems());

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
