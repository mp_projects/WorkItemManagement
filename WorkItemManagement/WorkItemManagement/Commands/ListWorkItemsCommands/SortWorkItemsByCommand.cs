﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands.ListWorkItemsCommands
{
    /// <author>Georgi Stefanov</author>
    class SortWorkItemsByCommand : Command
    {
        public SortWorkItemsByCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: sortby param asc/desc
            // param = one of the following: title / priority / severity / size / rating

            if (this.CommandParameters.Count < 1 || this.CommandParameters.Count > 2)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "sorting key and/or sorting type (asc/desc)"));
            }

            List<string> validSortKeys = new List<string> { "title", "priority", "severity", "size", "rating" };

            string sortType;
            //sortType = this.CommandParameters[1] ?? "asc";
            
            if (this.CommandParameters.Count == 1)
            {
                sortType = "asc";
            }
            else if (this.CommandParameters[1] == "asc" || this.CommandParameters[1] == "desc")
            {
                sortType = this.CommandParameters[1];
            }
            else
            {
                throw new ArgumentException("Invalid sorting type. Please eneter desc for descending or ommit for ascending");
            }

            StringBuilder itemsToString = this.CommandParameters[0].ToLower() switch
            {
                "title" => GetOutputSortByTitle(sortType),
                "priority" => GetOutputSortByPriority(sortType),
                "severity" => GetOutputSortBySeverity(sortType),
                "size" => GetOutputSortBySize(sortType),
                "rating" => GetOutputSortByRating(sortType),
                _ => throw new ArgumentException(string.Format(ErrorStrings.INVALID_SORTING_KEY, string.Join(", ", validSortKeys)))
            };

            //this.Writer.SetPrintColor(StringType.List);
            this.Writer.SetPrintColor(StringType.List);
            return itemsToString.ToString().Trim();
        }

        private StringBuilder GetOutputSortByTitle(string sortType)
        {
            IEnumerable<IWorkItem> sortedItems = this.Database.GetWorkItemsSortedByTitle(sortType);

            if (!sortedItems.Any())
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET,"work items"));
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of all work items sorted by title in [{sortType}] order:");
            sb.AppendLine($"{"Title",-51}{"Type",-9}");
            sb.Append(string.Join(Environment.NewLine, sortedItems.Select(i => $"{i.Title,-51}{i.GetType().Name,-9}").ToList()));

            return sb;
        }

        private StringBuilder GetOutputSortByPriority(string sortType)
        {
            IEnumerable<IBugStory> sortedItems = this.Database.GetWorkItemsSortedByPriority(sortType);

            if (!sortedItems.Any())
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "bugs/stories"));
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of all work items sorted by priority in [{sortType}] order:");
            sb.AppendLine($"{"Title",-51}{"Type",-9}{"Priority",-9}");
            sb.Append(string.Join(Environment.NewLine,
                sortedItems.Select(i => $"{i.Title,-51}{i.GetType().Name,-9}{i.Priority,-9}").ToList()));

            return sb;
        }

        private StringBuilder GetOutputSortBySeverity(string sortType)
        {
            IEnumerable<IBug> sortedItems = this.Database.GetWorkItemsSortedBySeverity(sortType);

            if (!sortedItems.Any())
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "bugs"));
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of all work items sorted by severity in [{sortType}] order:");
            sb.AppendLine($"{"Title",-51}{"Type",-9}{"Size",-9}");
            sb.Append(string.Join(Environment.NewLine,
                sortedItems.Select(i => $"{i.Title,-51}{i.GetType().Name,-9}{i.Priority,-9}").ToList()));

            return sb;
        }

        private StringBuilder GetOutputSortBySize(string sortType)
        {
            IEnumerable<IStory> sortedItems = this.Database.GetWorkItemsSortedBySize(sortType);

            if (!sortedItems.Any())
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "stories"));
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of all work items sorted by size in [{sortType}] order:");
            sb.AppendLine($"{"Title",-51}{"Type",-9}{"Size",-9}");
            sb.Append(string.Join(Environment.NewLine,
                sortedItems.Select(i => $"{i.Title,-51}{i.GetType().Name,-9}{i.Size,-9}").ToList()));

            return sb;
        }

        private StringBuilder GetOutputSortByRating(string sortType)
        {
            IEnumerable<IFeedback> sortedItems = this.Database.GetWorkItemsSortedByRating(sortType);

            if (!sortedItems.Any())
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "feedbacks"));
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of all work items sorted by rating in [{sortType}] order:");
            sb.AppendLine($"{"Title",-51}{"Type",-9}{"Raing",-9}");
            sb.Append(string.Join(Environment.NewLine,
                sortedItems.Select(i => $"{i.Title,-51}{i.GetType().Name,-9}{i.Rating,-9}").ToList()));

            return sb;
        }
    }
}
