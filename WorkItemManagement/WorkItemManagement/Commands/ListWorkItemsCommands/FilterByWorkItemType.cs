﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WorkItemManagement.Commands.ListWorkItemsCommands
{
    /// <author>Georgi Stefanov</author>
    class FilterByWorkItemType : Command
    {
        public FilterByWorkItemType(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: filterbyworkitemtype WorkItemType

            if (this.CommandParameters.Count != 1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "work item type"));
            }

            Type filterType = this.CommandParameters[0].ToLower() switch
            {
                "bug" => typeof(Bug),
                "story" => typeof(Story),
                "feedback" => typeof(Feedback),
                _ => throw new ArgumentException(ErrorStrings.INVALID_WORK_ITEM_TYPE)
            };

            List<IWorkItem> items = this.Database.GetWorkItemsFilteredByType(filterType).ToList();

            string workItemTypeInHeader = this.CommandParameters[0].ToLower() switch
            {
                "bug" => "bugs",
                "story" => "stories",
                "feedback" => "feedbacks",
                _ => ""
            };

            if (items.Count == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, workItemTypeInHeader));

                //NO_WORK_ITEMS_YET = "There are still no {0} created."
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"List of {workItemTypeInHeader}:");
            sb.Append(string.Join(Environment.NewLine, items));

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();
        }
    }
}
