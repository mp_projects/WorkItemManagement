﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Commands.ChangeBugCommands
{
    class ChangeBugStatusCommand : Command
    {
        /// <author>Georgi Stefanov</author>
        public ChangeBugStatusCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: changebugpriority Team Board Bug NewStatus
            if (this.CommandParameters.Count != 4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, bug title and new status"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);
            var bug = board.GetBugByTitle(this.CommandParameters[2]);

            if (!Enum.TryParse(this.CommandParameters[3], out BugStatus status))
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_WORK_ITEM_LABEL,
                    "Bug",
                    "status ",
                    "Active, Fixed"
                    ));
            }

            if (bug.Status == status)
            {
                this.Writer.SetPrintColor(StringType.Neutral);
                return string.Format(Strings.PROPERTY_NOT_CHANGED, "Status", bug.Status);
            }
            else
            {
                BugStatus current = bug.Status;

                bug.ChangeStatus(status);

                board.EventLog.AddEvent(new Event(
                    string.Format(EventStrings.WI_PROPERTY_CHANGED, "Bug", bug.Title, "status", current, bug.Status)));

                this.Writer.SetPrintColor(StringType.Success);
                return string.Format(SuccessfullStrings.WORK_ITEM_CHANGED, "Bug", $"{bug.Title}", "status", status);
            }
        }
    }
}
