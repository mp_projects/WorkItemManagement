﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Commands.ChangeBugCommands
{
    class ChangeBugPriorityCommand : Command
    {
        /// <author>Georgi Stefanov</author>
        public ChangeBugPriorityCommand(IList<string> commandParameters, IDatabase database, IWriter writer)
            : base(commandParameters, database,writer)
        {

        }

        public override string Execute()
        {
            // COMMAND SINTAXIS: changebugpriority Team Board Bug NewPriority
            if (this.CommandParameters.Count != 4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, bug title and new priority"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);
            var bug = board.GetBugByTitle(this.CommandParameters[2]);

            if (!Enum.TryParse(this.CommandParameters[3], out Priority priority))
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_WORK_ITEM_LABEL,
                    "Bug",
                    "priority",
                    "Lo, Medium, High"
                    ));
            }

            if (bug.Priority == priority)
            {
                this.Writer.SetPrintColor(StringType.Neutral);
                return string.Format(Strings.PROPERTY_NOT_CHANGED, "Priority", bug.Priority);
            }
            else
            {
                Priority current = bug.Priority;
                
                bug.ChangePriority(priority);

                board.EventLog.AddEvent(new Event(
                    string.Format(EventStrings.WI_PROPERTY_CHANGED, "Bug", bug.Title, "priority", current, bug.Priority)));

                this.Writer.SetPrintColor(StringType.Success);
                return string.Format(
                    SuccessfullStrings.WORK_ITEM_CHANGED, "Bug", $"{bug.Title}", "priority", priority);
            }
        }
    }
}
