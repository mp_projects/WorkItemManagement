﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.WorkItems;

namespace WorkItemManagement.Commands.CreateWorkItem
{
    /// <author>Mihail Popov</author>
    public class CreateFeedbackInBoardCommand : Command
    {
        public CreateFeedbackInBoardCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if(this.CommandParameters.Count!=4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team, board, work item title and work item description"));
            }

            var team = Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);

            //board.CheckWorkItemExistance(typeof(Feedback), this.CommandParameters[2]);
            board.CheckWorkItemExistance("feedback", this.CommandParameters[2]);

            var feedback = this.Factory.CreateFeedbackInBoard(
                 title: this.CommandParameters[2],
                 description: this.CommandParameters[3]
                );

            board.AddWorkItem(feedback);

            feedback.RecordTeamAndBoard(team, board);

            //this.Database.WorkItems.Add(feedback);

            this.Database.AddWorkItem(feedback);

            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(
                SuccessfullStrings.WORK_ITEM_ADDED_IN_BOARD, "Feedback", feedback.Title, team.Name, board.Name);
        }
    }
}
