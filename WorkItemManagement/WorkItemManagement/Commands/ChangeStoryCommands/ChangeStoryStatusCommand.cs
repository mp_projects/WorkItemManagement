﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Commands.ChangeStoryCommands
{
    /// <author>Mihail Popov</author>
    public class ChangeStoryStatusCommand : Command
    {
        public ChangeStoryStatusCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {

        }
        public override string Execute()
        {
            if (this.CommandParameters.Count != 4)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name, board name, story title and the new status"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);
            var board = team.GetBoard(this.CommandParameters[1]);
            var story = board.GetStoryByTitle(this.CommandParameters[2]);

            if (!Enum.TryParse(this.CommandParameters[3], out StoryStatus newStatus))
            {
                throw new ArgumentException(ErrorStrings.INVALID_STORY_STATUS);
            }

            if(story.Status == newStatus)
            {
                this.Writer.SetPrintColor(StringType.Neutral);
                return string.Format(Strings.PROPERTY_NOT_CHANGED, "Status", story.Status);

                //throw new ArgumentException(string.Format(Strings.PROPERTY_NOT_CHANGED, "Status", story.Status));
            }

            StoryStatus current = story.Status;

            story.ChangeStatus(newStatus);

            board.EventLog.AddEvent(new Event(
                string.Format(EventStrings.WI_PROPERTY_CHANGED, story.Title, "Story", "status", current, story.Status)));

            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(
                SuccessfullStrings.WORK_ITEM_CHANGED, "Story", $"{story.Title}", "status", newStatus);
        }
    }
}
