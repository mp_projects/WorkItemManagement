﻿using WorkItemManagement.Commands.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Core;
using System.Text;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Commands
{
    /// <author>Mihail Popov</author>
    public class ShowAllTeamMembersCommand : Command
    {
        public ShowAllTeamMembersCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name"));
            }

            var team = this.Database.GetTeam(this.CommandParameters[0]);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Team \"{team.Name}\" members list:");
            sb.AppendLine(team.ShowMembers());

            this.Writer.SetPrintColor(StringType.List);
            return sb.ToString().Trim();

        }
    }
}
