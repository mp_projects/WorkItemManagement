﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;

namespace WorkItemManagement.Commands
{
    /// <author>Mihail Popov</author>
    public class CreateTeamCommand : Command
    {
        public CreateTeamCommand(IList<string> commandParameters, IDatabase database, IWriter writer) 
            : base(commandParameters, database,writer)
        {
        }

        public override string Execute()
        {
            if(this.CommandParameters.Count!=1)
            {
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_PARAMETERS_NUMBER, "team name"));
            }

            string teamName = this.CommandParameters[0];

            this.Database.CheckTeamExistance(teamName);

            Team team = this.Factory.CreateTeam(teamName);

            this.Database.AddTeam(team);


            this.Writer.SetPrintColor(StringType.Success);
            return string.Format(SuccessfullStrings.CREATED_TEAM, teamName);
        }
    }
}
