﻿using System;
using WorkItemManagement.Models;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement
{
    class Program
    {

        static void Main(string[] args)
        {

            Engine.Instance.Run();

        }

    }
}
