﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Abstracts
{
    /// <author>Georgi Stefanov</author>
    public abstract class WorkItem : IWorkItem
    {
        private const int TITLE_MIN_LENGTH = 10;
        private const int TITLE_MAX_LENGTH = 50;
        private const int DESCR_MIN_LENGTH = 10;
        private const int DESCR_MAX_LENGTH = 500;

        private string title;
        private string description;
        private List<Comment> comments;

        protected ITeam team;
        protected IBoard board;
        

        protected WorkItem(string title, string description)
        {
            this.Title = title;

            this.Description = description;

            this.EventLog = new EventLog();
            this.comments = new List<Comment>();

            this.ID++;

        }

        public int ID { get; private set; }

        public string Title
        {
            get
            {
                return this.title;
            }
            private set
            {
                ValidateStringLength(value, "Title", TITLE_MIN_LENGTH, TITLE_MAX_LENGTH);
                this.title = value;
            }
        }

        public string Description
        {
            get
            {
                return this.description;
            }
            private set
            {
                ValidateStringLength(value, "Description", DESCR_MIN_LENGTH, DESCR_MAX_LENGTH);

                this.description = value;
            }
        }

        public IReadOnlyList<Comment> Comments
        {
            get
            {
                return this.comments;
            }
        }

        public IEventLog EventLog { get; }

        public void AddComment(Comment comment)
        {
            this.comments.Add(comment);

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.COMMENT_ADDED, comment.Author.Name)));
        }

        private void ValidateStringLength(string stringContents, string stringName, int minLength, int maxLength)
        {
            if (stringContents == null || stringContents.Length < minLength || stringContents.Length > maxLength)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_NAME_LENGTH, stringName, minLength, maxLength));
            }
        }

        public void RecordTeamAndBoard(ITeam team, IBoard board)
        {
            this.team ??= team;
            this.board ??= board;
        }

        public override bool Equals(object obj)
        {
            if (obj is WorkItem)
            {
                WorkItem otherItem = (WorkItem)obj;

                if (this.Title == otherItem.Title && this.GetType() == otherItem.GetType())
                {
                    return true;
                }
            }            
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"{this.Title}";
        }

    }
}
