﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models
{
    /// <author>Mihail Popov</author>
    public class Team : ITeam
    {
        private const int NAME_MIN_LENGTH = 2;
        private const int NAME_MAX_LENGTH = 20;
        private string name;
        private List<IBoard> boards;
        private List<IMember> members;
        private static int id;

        public Team(string name)
        {
            this.ValidateName(name);

            this.name = name;
            this.boards = new List<IBoard>();
            this.members = new List<IMember>();
            this.EventLog = new EventLog();
            this.ID = ++id;

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.TEAM_CREATED)));
        }

        public string Name
        {
            get => this.name;

            set
            {
                this.ValidateName(value);

                this.EventLog.AddEvent(new Event(string.Format(EventStrings.NAME_CHANGED, name, value)));

                this.name = value;
            }
        }

        public IReadOnlyList<IMember> Members { get => this.members; }

        public IReadOnlyList<IBoard> Boards { get => this.boards; }

        public IEventLog EventLog { get; }

        public int ID { get; }

        public void AddTeammate(IMember member)
        {
            if (this.Members.Contains(member))
            {
                throw new ArgumentException(string.Format(ErrorStrings.MEMBER_EXIST, member.Name, this.Name));
            }

            this.members.Add(member);
            member.AddTeam(this);

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_ADDED, member.Name)));
        }

        public string ShowMembers()
        {
            if (this.Members.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_MEMBERS_YET);
            }

            return string.Join(Environment.NewLine, this.Members);
        }

        public string ShowActivity()
        {
            return string.Join(Environment.NewLine, this.EventLog.History);
        }

        public string ShowBoards()
        {
            if (this.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            return string.Join(Environment.NewLine, this.Boards);
        }

        public IBoard GetBoard(string boardName)
        {
            var board = this.Boards
                .Where(board => board.Name == $"{boardName}")
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_TEAM_BOARD, boardName, this.Name));

            return board;
        }

        public IMember GetMember(string memberName)
        {
            var member = this.Members
                .Where(member => member.Name == memberName)
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_TEAM_MEMBER, memberName, this.Name));

            return member;
        }

        public void AddBoard(IBoard board)
        {
            if (this.Boards.Contains(board))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", board.Name));
            }
            
            this.boards.Add(board);

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.BOARD_ADDED,board.Name)));
        }

        public void CheckBoardExistance(string boardName)
        {
            if (this.Boards.Select(board => board.Name).Contains(boardName))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", boardName));
            }
        }

        public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
        {
            List<IFeedback> filteredFeedbackItems = new List<IFeedback>();

            if (board != null)
            {
                CheckBoardExistance(board.Name);

                var boardFeedbacks = board.GetAllFeedbacks();

                if (boardFeedbacks.Count() == 0)
                {
                    throw new ArgumentException(
                        string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "feedbacks"));
                }

                filteredFeedbackItems = boardFeedbacks
                   .Where(item => item.Status == status)
                   .ToList();

                if(filteredFeedbackItems.Count==0)
                {
                    throw new ArgumentException(ErrorStrings.INVALID_FILTRATION);
                }

                return filteredFeedbackItems;
            }

            if (this.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            this.Boards.ToList()
                .ForEach(board =>
                {
                    if (board.GetAllFeedbacks().Count() != 0)
                    {
                        filteredFeedbackItems
                       .AddRange(
                            board.WorkItems
                            .Where(item => item is IFeedback)
                            .Select(item => item as IFeedback)
                            .Where(item => item.Status == status)
                            .ToList()
                              );
                    }
                });

            if (filteredFeedbackItems.Count() == 0)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.FEEDBACK_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredFeedbackItems;
        }

        public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
        {
            List<IBug> filteredBugItems = new List<IBug>();

            if (board != null)
            {
                var boardBugs = board.GetAllBugs();

                if (boardBugs.Count() == 0)
                {
                    throw new ArgumentException(
                        string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "bugs"));
                }

                filteredBugItems = boardBugs
                   .Where(item => item.Status == status)
                   .ToList();

                if (filteredBugItems.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.INVALID_FILTRATION);
                }

                return filteredBugItems;
            }

            if (this.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            this.Boards.ToList()
                .ForEach(board =>
                {
                    if (board.GetAllBugs().Count() != 0)
                    {
                        filteredBugItems
                        .AddRange(
                            board.WorkItems
                            .Where(item => item is IBug)
                            .Select(item => item as IBug)
                            .Where(item => item.Status == status)
                            );
                    }
                });

            if (filteredBugItems.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.BUGS_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredBugItems;
        }

        public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
        {
            List<IStory> filteredStoryItems = new List<IStory>();

            if (board != null)
            {
                var boardStories = board.GetAllStories();

                if (boardStories.Count() == 0)
                {
                    throw new ArgumentException(
                        string.Format(ErrorStrings.NO_WORK_ITEMS_IN_BOARD, "stories"));
                }

                filteredStoryItems = boardStories
                   .Where(item => item.Status == status)
                   .ToList();

                if (filteredStoryItems.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.INVALID_FILTRATION);
                }

                return filteredStoryItems;
            }

            if (this.Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            this.Boards.ToList()
                .ForEach(board =>
                {
                    if (board.WorkItems.Where(item => item is IStory).Count() != 0)
                    {
                        filteredStoryItems
                        .AddRange(
                            board.WorkItems
                            .Where(item => item is IStory)
                            .Select(item => item as IStory)
                            .Where(item => item.Status == status)
                            .ToList()
                            );
                    }
                });

            if (filteredStoryItems.Count() == 0)
            {
                throw new ArgumentException(
                                    string.Format(ErrorStrings.STORIES_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredStoryItems;
        }

        public string ListAllItems()
        {
            if (Boards.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
            }

            List<IWorkItem> allBoardsItems = new List<IWorkItem>();

            this.Boards.ToList().ForEach(board => allBoardsItems.AddRange(board.WorkItems));

            if (allBoardsItems.Count == 0)
            {
                throw new ArgumentException(ErrorStrings.NO_WORKITEMS_IN_TEAM_YET);
            }

            return string.Join(Environment.NewLine,allBoardsItems.Select(wi => $"{wi.GetType().Name,-10}{wi}"));
        }

        public void CheckMemberExistance(string memberName)
        {
            if (this.Members.Select(member => member.Name).Contains(memberName))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Member", memberName));
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        private void ValidateName(string name)
        {
            if (name == null || name.Length < NAME_MIN_LENGTH || name.Length > NAME_MAX_LENGTH)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.INVALID_NAME_LENGTH, "Team", NAME_MIN_LENGTH, NAME_MAX_LENGTH));
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is Team)
            {
                Team otherMember = (Team)obj;

                if (this.Name == otherMember.Name)
                {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }


    }
}