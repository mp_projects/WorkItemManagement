﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Georgi Stefanov</author>
    public interface IStory : IWorkItem, IBugStory
    {
        public StoryStatus Status { get; }

        public StorySize Size { get; }

        /// <summary>
        /// Changes the story priority and writes into EvenetLog
        /// </summary>
        /// <param name="status">The new priority</param>
        public void ChangePriority(Priority priority);

        /// <summary>
        /// Changes the story size and writes into EvenetLog
        /// </summary>
        /// <param name="status">The new size</param>
        public void ChangeSize(StorySize size);

        /// <summary>
        /// Changes the story status and writes into EvenetLog
        /// </summary>
        /// <param name="status">The new status</param>
        public void ChangeStatus(StoryStatus status);

    }
}
