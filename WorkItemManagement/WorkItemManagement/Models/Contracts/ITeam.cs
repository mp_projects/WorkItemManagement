﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Mihail Popov</author>
    public interface ITeam
    { 
        public string Name { get; }

        public IReadOnlyList<IMember> Members { get; }

        public IReadOnlyList<IBoard> Boards { get; }

        public IEventLog EventLog { get; }

        public int ID { get; }

        public void AddTeammate(IMember member);
        public string ShowMembers();

        public string ShowActivity();

        public string ShowBoards();

        public IBoard GetBoard(string boardName);

        public IMember GetMember(string memberName);

        public void AddBoard(IBoard board);

        public void CheckBoardExistance(string boardName);

        public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null);

        public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null);

        public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null);

        public string ListAllItems();

        public void CheckMemberExistance(string memberName);
    }
}
