﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Mihail Popov</author>
    public interface IBoard
    {
        public string Name { get; }
      
        public EventLog EventLog { get; }

        public IReadOnlyList<IWorkItem> WorkItems { get; }

        public int ID { get; }

        public void AddWorkItem(IWorkItem item);

        public string ShowActivity();

        public IEnumerable<IFeedback> GetAllFeedbacks();

        public IEnumerable<IBug> GetAllBugs();

        public IEnumerable<IStory> GetAllStories();

        public IWorkItem GetWorkItemByTypeAndTitle(string type, string title);

        public void CheckWorkItemExistance(string type, string title);

        public IFeedback GetFeedbackByTitle(string feedbackTitle);

        public IBug GetBugByTitle(string bugTitle);

        public IStory GetStoryByTitle(string storyTitle);

        public string ListAllItems();
    }
}
