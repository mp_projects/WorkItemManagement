﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Mihail Popov</author>
    public interface IEventLog
    {
        public IReadOnlyList<Event> History { get; }

        public void AddEvent(Event newEvent);
    }
}
