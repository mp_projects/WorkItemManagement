﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Enums;

namespace WorkItemManagement.Models.Contracts
{
    /// <author>Georgi Stefanov</author>
    public interface IWorkItem
    {
        int ID { get; }

        string Title { get; }

        string Description { get; }

        IReadOnlyList<Comment> Comments { get; }

        IEventLog EventLog { get; }

        /// <summary>
        /// Adds comment into comments list of the work item and writes into its EventLog
        /// </summary>
        /// <param name="comment">The comment object to add</param>
        void AddComment(Comment comment);

        /// <summary>
        /// Initializes the readonly fileds team and board to which the work itemn belongs. Once initialized they can not be changed.
        /// </summary>
        /// <param name="team">The team object</param>
        /// <param name="board">The board object</param>
        void RecordTeamAndBoard(ITeam team, IBoard board);

        /// <summary>
        /// Returns the workitem title as string
        /// </summary>
        /// <returns>String containing the title of the work item</returns>
        string ToString();

    }
}
