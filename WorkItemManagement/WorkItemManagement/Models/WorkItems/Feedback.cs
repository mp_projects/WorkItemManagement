﻿using WorkItemManagement.Core;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Abstracts;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Models.WorkItems
{
    /// <author>Georgi Stefanov</author>
    public class Feedback : WorkItem, IFeedback
    {
        public Feedback(string title, string description)
            : base(title, description)
        {
            this.Status = FeedbackStatus.New;
            this.Rating = 0;

            EventLog.AddEvent(new Event(string.Format(EventStrings.WORK_ITEM_CREATED, "Feedback")));
        }

        public FeedbackStatus Status { get; private set; }

        public int Rating { get; private set; }

        public void ChangeStatus(FeedbackStatus status)
        {
            var current = this.Status;
            this.Status = status;
            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED,  "Status", current, this.Status)));
        }

        public void ChangeRating(int rating)
        {
            if (rating < 0)
            {
                throw new ArgumentException(ErrorStrings.INVALID_RATING);
            }

            var current = this.Rating;
            this.Rating = rating;
            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED,  "Rating", current, this.Rating)));
        }

    }
}
