﻿using WorkItemManagement.Core;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models.Abstracts;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Core.Strings;
using System.Linq;

namespace WorkItemManagement.Models.WorkItems
{
    /// <author>Georgi Stefanov</author>
    public class Bug : WorkItem, IBug
    {
        private List<IMember> assignedMembers = new List<IMember>();
        private List<string> stepsToReproduce = new List<string>();

        public Bug(string title, string description, Priority priority, BugSeverity severity, List<string> stepsToReproduce)
            : base(title, description)
        {
            this.Status = BugStatus.Active;
            this.Priority = priority;
            this.Severity = severity;
            this.stepsToReproduce = stepsToReproduce;

            EventLog.AddEvent(new Event(string.Format(EventStrings.WORK_ITEM_CREATED, "Bug")));
        }

        public BugStatus Status { get; private set; }

        public Priority Priority { get; private set; }

        public IReadOnlyList<string> StepsToReproduce { get => this.stepsToReproduce; }

        public BugSeverity Severity { get; private set; }

        public IReadOnlyList<IMember> Assignees { get => this.assignedMembers;  }

        public void AddAssignee(IMember assignee)
        {
            if (!this.assignedMembers.Contains(assignee))
            {
                this.assignedMembers.Add(assignee);
                EventLog.AddEvent(new Event(string.Format(EventStrings.ASSIGNEE_ADDED, assignee.Name)));
            }
        }

        public void RemoveAssignee(IMember assignee)
        {
            if (this.assignedMembers.Contains(assignee))
            {
                this.assignedMembers.Remove(assignee);
                EventLog.AddEvent(new Event(string.Format(EventStrings.ASSIGNEE_REMOVED, assignee.Name)));
            }
        }

        public void ChangeSeverity(BugSeverity severity)
        {
            var current = this.Severity;
            this.Severity = severity;

            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED,  "Severity", current, this.Severity)));
        }

        public void ChangeStatus(BugStatus status)
        {
            var current = this.Status;
            this.Status = status;
            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED,  "Status", current, this.Status)));
        }

        public void ChangePriority(Priority priority)
        {
            var current = this.Priority;
            this.Priority = priority;
            this.EventLog.AddEvent(new Event(
                string.Format(EventStrings.PROPERTY_CHANGED, "Priority", current, this.Priority)));
        }
    }
}
