﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Enums
{
    public enum Priority
    {
        Low = 1,
        Medium = 2,
        High = 3
    }
}
