﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Models.Enums
{
    public enum StorySize
    {
        Small = 1,
        Medium = 2,
        Large = 3
    }
}
