﻿using WorkItemManagement.Core.Contracts;
using System.Collections.Generic;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using System.Linq;
using System;
using WorkItemManagement.Models.Abstracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;
using System.IO;

namespace WorkItemManagement.Core
{

    public class Database : IDatabase
    {
        private static IDatabase instance = null;
        private List<IMember> members = new List<IMember>();
        private List<ITeam> teams = new List<ITeam>();
        private List<IWorkItem> workItems = new List<IWorkItem>();

        public static IDatabase Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Database();
                }

                return instance;
            }
        }

        private Database()
        {

        }

        public IReadOnlyList<ITeam> Teams { get => teams; }

        public IReadOnlyList<IMember> Members { get => members; }

        public IReadOnlyList<IWorkItem> WorkItems { get => workItems; }

        public void AddMember(IMember member)
        {
            this.members.Add(member);
        }

        public void AddTeam(ITeam team)
        {
            this.teams.Add(team);
        }

        public void AddWorkItem(IWorkItem workItem)
        {
            this.workItems.Add(workItem);
        }

        public ITeam GetTeam(string teamName)
        {
            var team = this.Teams
               .Where(team => team.Name == teamName)
               .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.OBJECT_DOES_NOT_EXIST, "Team", teamName));

            return team;
        }

        public IMember GetMember(string memberName)
        {
            var member = this.Members
                .Where(m => m.Name == memberName)
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.OBJECT_DOES_NOT_EXIST, "Member", memberName));

            return member;
        }

        public string ListAllWorkItems()
        {
            if (this.WorkItems.Count == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "workitems"));
            }

            return string.Join(Environment.NewLine, this.WorkItems.Select(wi => $"{wi.GetType().Name,-10}{wi}"));
        }

        public void CheckMemberExistance(string memberName)
        {
            if (this.Members.Select(member => member.Name).Contains(memberName))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Member", memberName));
            }
        }

        public void CheckTeamExistance(string teamName)
        {
            if (Teams.Select(team => team.Name).Contains(teamName))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Team", teamName));
            }
        }

        public IEnumerable<IWorkItem> GetWorkItemsFilteredByType(Type filterType)
        {
            return this.WorkItems.Where(wi => wi.GetType().Equals(filterType)).ToList();
        }

        public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status)
        {
            var storyItems = this.WorkItems.Where(item => item is IStory);

            if (storyItems.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "stories"));
            }

            var filtratedStoryItems = storyItems
                  .Select(item => item as IStory)
                  .Where(item => item.Status == status)
                  .ToList();

            if (filtratedStoryItems.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.STORIES_WITH_STATUS_NOT_FOUND, status));
            }

            return filtratedStoryItems;
        }

        public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status)
        {
            var bugItems = this.WorkItems.Where(item => item is IBug);

            if (bugItems.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "stories"));
            }

            var filtratedBugItems = bugItems
                 .Select(item => item as IBug)
                 .Where(item => item.Status == status)
                 .ToList();

            if (filtratedBugItems.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.BUGS_WITH_STATUS_NOT_FOUND, status));
            }

            return filtratedBugItems;
        }

        public IEnumerable<IFeedback> GetWorkItemsFilteredBy(FeedbackStatus status)
        {
            var feedbackItems = this.WorkItems.Where(item => item is IFeedback);

             if (feedbackItems.Count() == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "stories"));
            }

            var filteredFeedbackItems = feedbackItems
               .Select(item => item as IFeedback)
               .Where(item => item.Status == status)
               .ToList();

            if (filteredFeedbackItems.Count() == 0)
            {
                throw new ArgumentException(
                    string.Format(ErrorStrings.FEEDBACK_WITH_STATUS_NOT_FOUND, status));
            }

            return filteredFeedbackItems;
        }

        public IEnumerable<IWorkItem> GetWorkItemsSortedByTitle(string sortType)
        {
            return sortType switch
            {
                "asc" => this.WorkItems.OrderBy(i => i.Title),
                "desc" => this.WorkItems.OrderByDescending(i => i.Title),
                _ => throw new ArgumentException()
            };
        }

        public IEnumerable<IBugStory> GetWorkItemsSortedByPriority(string sortType)
        {
            return sortType switch
            {
                "asc" => this.WorkItems
                            .Where(i => i is IBugStory).Select(i => i as IBugStory)
                            .OrderBy(i => i.Priority),
                "desc" => this.WorkItems
                            .Where(i => i is IBugStory).Select(i => i as IBugStory)
                            .OrderByDescending(i => i.Priority),
                _ => throw new ArgumentException()
            };
        }

        public IEnumerable<IBug> GetWorkItemsSortedBySeverity(string sortType)
        {
            return sortType switch
            {
                "asc" => this.WorkItems
                        .Where(i => i is IBug).Select(i => i as IBug)
                        .OrderBy(i => i.Severity),
                "desc" => this.WorkItems
                        .Where(i => i is IBug).Select(i => i as IBug)
                        .OrderByDescending(i => i.Severity),
                _ => throw new ArgumentException()
            };

        }

        public IEnumerable<IStory> GetWorkItemsSortedBySize(string sortType)
        {
            return sortType switch
            {
                "asc" => this.WorkItems
                        .Where(i => i is IStory).Select(i => i as IStory)
                        .OrderBy(i => i.Size),
                "desc" => this.WorkItems
                        .Where(i => i is IStory).Select(i => i as IStory)
                        .OrderByDescending(i => i.Size),
                _ => throw new ArgumentException()
            };

        }

        public IEnumerable<IFeedback> GetWorkItemsSortedByRating(string sortType)
        {
            return sortType switch
            {
                "asc" => this.WorkItems
                        .Where(i => i is IFeedback).Select(i => i as IFeedback)
                        .OrderBy(i => i.Rating),
                "desc" => this.WorkItems
                        .Where(i => i is IFeedback).Select(i => i as IFeedback)
                        .OrderByDescending(i => i.Rating),
                _ => throw new ArgumentException()
            };

        }
    }
}
