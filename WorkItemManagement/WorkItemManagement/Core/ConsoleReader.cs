﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Contracts;

namespace WorkItemManagement.Core
{
    class ConsoleReader : IReader
    {
        public string Read()
        {
            string input = Console.ReadLine();
            return input;
        }
    }
}
