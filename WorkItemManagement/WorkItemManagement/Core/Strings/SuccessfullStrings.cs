﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Core.Strings
{
    public static class SuccessfullStrings
    {
        public const string WORK_ITEM_ADDED_IN_BOARD = "{0} \"{1}\" sucessfully added in team {2}, board {3}";
        public const string WORK_ITEM_CHANGED = "{0} \"{1}\" {2} successfully changed to {3}";
        public const string ASSIGNED_WORK_ITEM = "Work item \"{0}\" successfully assigned to {1}";
        public const string UNASSIGNED_WORK_ITEM = "Work item \"{0}\" sucessfully unassigned from {1}";
        public const string PROPERTY_CHANGED = "{0} successfully changed";
        public const string COMMENT_ADDED = "Member {0} successfully added comment to {1} \"{2}\" in bard \"{3}\" in team \"{4}\".";
        public const string CREATED_MEMBER = "Successfully created member: {0}";
        public const string CREATED_TEAM = "Successfully created team: {0}";
    }
}
