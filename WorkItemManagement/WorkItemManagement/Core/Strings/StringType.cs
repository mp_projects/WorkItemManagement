﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Core.Strings
{
    public enum StringType
    {
        Success,
        Error,
        List,
        Activity,
        Neutral
    }
}
