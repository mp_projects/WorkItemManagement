﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Commands.Contracts;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Core
{
    public class Processor
    {
        private readonly ICommandManager commandManager;

        public Processor()
        {
            commandManager = new CommandManager();
        }

        public string Process(string commandLine, IDatabase database, IWriter writer)
        {
            try
            {

                ICommand command = this.commandManager.ParseCommand(commandLine, database,writer);
                string result = command.Execute();

                return result.Trim();
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                {
                    e = e.InnerException;
                }

                writer.SetPrintColor(StringType.Error);
                return $"ERROR: {e.Message}";
            }
        }
    }
}
