﻿using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Strings;

namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov, Georgi Stefanov</author>
    public interface IWriter
    {
        /// <summary>
        /// Sets the color which will be used to print on the console based on the type of message.
        /// </summary>
        /// <param name="stringType">The message type</param>
        void SetPrintColor(StringType stringType);

        /// <summary>
        /// Prints the command result on the console using the set color.
        /// Returns the forrground color to the previous state.
        /// </summarys>
        /// <param name="commandResult"></param>
        void Print(string commandResult);
    }
}
