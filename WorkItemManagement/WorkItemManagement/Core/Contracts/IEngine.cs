﻿namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov, Georgi Stefanov</author>
    public interface IEngine
    {
        void Run();
    }
}
