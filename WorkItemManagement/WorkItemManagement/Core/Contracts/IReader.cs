﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkItemManagement.Core.Contracts
{
    /// <author>Mihail Popov, Georgi Stefanov</author>
    interface IReader
    {
        /// <summary>
        /// Reads the command line string from the console.
        /// </summary>
        /// <returns></returns>
        string Read();
    }
}
