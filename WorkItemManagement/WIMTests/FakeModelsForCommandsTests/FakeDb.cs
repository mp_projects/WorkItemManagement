﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.FakeModelsForCommandsTests
{
    public class FakeDb : IDatabase
    {

        private readonly List<IMember> members = new List<IMember>();
        private readonly List<ITeam> teams = new List<ITeam>();
        private readonly List<IWorkItem> workItems = new List<IWorkItem>();

        public IReadOnlyList<ITeam> Teams { get => teams; }

        public IReadOnlyList<IMember> Members { get => members; }

        public IReadOnlyList<IWorkItem> WorkItems { get =>workItems; }

        public void AddMember(IMember member)
        {
            throw new NotImplementedException();
        }

        public void AddTeam(ITeam team)
        {
            this.teams.Add(team);
        }

        public void AddWorkItem(IWorkItem workItem)
        {
            throw new NotImplementedException();
        }

        public void CheckMemberExistance(string memberName)
        {
            throw new NotImplementedException();
        }

        public void CheckTeamExistance(string team)
        {
            if (Teams.Select(team => team.Name).Contains(team))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Team", team));
            }
        }

        public IMember GetMember(string memberName)
        {
            var member = this.Members
                .Where(m => m.Name == memberName)
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.OBJECT_DOES_NOT_EXIST, "Member", memberName));

            return member;
        }

        public ITeam GetTeam(string teamName)
        {
            var team = this.Teams
               .Where(team => team.Name == teamName)
               .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.OBJECT_DOES_NOT_EXIST, "Team", teamName));

            return team;
        }

        public IEnumerable<IFeedback> GetWorkItemsFilteredBy(FeedbackStatus status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IWorkItem> GetWorkItemsFilteredByType(Type type)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IBugStory> GetWorkItemsSortedByPriority(string sortType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IFeedback> GetWorkItemsSortedByRating(string sortType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IBug> GetWorkItemsSortedBySeverity(string sortType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IStory> GetWorkItemsSortedBySize(string sortType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IWorkItem> GetWorkItemsSortedByTitle(string sortType)
        {
            throw new NotImplementedException();
        }

        public string ListAllWorkItems()
        {
            if (this.WorkItems.Count == 0)
            {
                throw new ArgumentException(string.Format(ErrorStrings.NO_WORK_ITEMS_YET, "workitems"));
            }

            return string.Join(Environment.NewLine, this.WorkItems);
        }

        public string ShowAllMembers()
        {
            throw new NotImplementedException();
        }

        public string ShowAllTeams()
        {
            throw new NotImplementedException();
        }
    }
}
