﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.FakeModelsForCommandsTests
{
    public class FakeTeam : ITeam
    {
        private List<IMember> members = new List<IMember>();
        private List<IBoard> boards = new List<IBoard>();

        public string Name => "testTeam";

        public IReadOnlyList<IMember> Members { get => this.members; }

        public IReadOnlyList<IBoard> Boards { get => this.boards; }

        public IEventLog EventLog { get; } = new EventLog();

        public int ID => throw new NotImplementedException();

        public void AddBoard(IBoard board)
        {
            if (this.boards.Contains(board))
            {
                throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", board.Name));
            }

            this.boards.Add(board);

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.BOARD_ADDED, board.Name)));
        }

        public void AddTeammate(IMember member)
        {
            if (this.members.Contains(member))
            {
                throw new ArgumentException(string.Format(ErrorStrings.MEMBER_EXIST, member.Name, this.Name));
            }

            this.members.Add(member);

            this.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_ADDED, member.Name)));
        }

        public void CheckBoardExistance(string boardName)
        {
            throw new NotImplementedException();
        }

        public void CheckMemberExistance(string memberName)
        {
            throw new NotImplementedException();
        }

        public IBoard GetBoard(string boardName)
        {
            var board = this.Boards
                .Where(board => board.Name == $"{boardName}")
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_TEAM_BOARD, boardName, this.Name));

            return board;
        }

        public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
        {
            throw new NotImplementedException();
        }

        public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
        {
            throw new NotImplementedException();
        }

        public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
        {
            throw new NotImplementedException();
        }

        public IMember GetMember(string memberName)
        {
            var member = this.Members
                .Where(member => member.Name == memberName)
                .FirstOrDefault() ??
                throw new ArgumentException(string.Format(ErrorStrings.INVALID_TEAM_MEMBER, memberName, this.Name));

            return member;
        }

        public string ListAllItems()
        {
            throw new NotImplementedException();
        }

        public string ShowActivity()
        {
            throw new NotImplementedException();
        }

        public string ShowBoards()
        {
            throw new NotImplementedException();
        }

        public string ShowMembers()
        {
            throw new NotImplementedException();
        }
    }
}
