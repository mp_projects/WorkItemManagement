﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands.ListWorkItemsCommands;

namespace WIMTests.CommandsTests.ListWorkItemsWithOptionsCommandsTests
{
    [TestClass]
    public class FilterWorkItemsByStatusCommand_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
        public void ThrowWhen_ParametersNumberInvalid()
        {
            //arrange
            List<string> commandParams = new List<string>() { "wrongParam" };

            //Act & Assert
            new FilterTeamItemsByStatusCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Such team does not exists.")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            //Arrange
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testWorkItemType",
                "testStatus"
            };

            //Act && Assert
            new FilterTeamItemsByStatusCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Such board does not exists in this team.")]
        public void ThrowWhen_BoardDoesNotExist()
        {
            //Arrange
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testWorkItemType",
                "testStatus"
            };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());

            //Assert
            new FilterTeamItemsByStatusCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid work item type")]
        public void ThrowWhen_WorkItemTypeInvalid()
        {
            //Arrange
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "wrongWorkItemType",
                "testStatus"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            fakeDb.AddTeam(fakeTeam);
            fakeTeam.AddBoard(new FakeBoard());

            //Assert
            new FilterTeamItemsByStatusCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Wrong bug status")]
        public void ThrowWhen_ParticularWorkItemTypeStatus()
        {
            //Arrange
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "bug",
                "wrongStatus"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            fakeDb.AddTeam(fakeTeam);
            fakeTeam.AddBoard(new FakeBoard());

            //Assert
            new FilterTeamItemsByStatusCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }
    }
}
