﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Core.Strings;

namespace WIMTests.CommandsTests
{
    [TestClass]
    public class CreateMemberCommand_Should
    {
        [TestMethod]
        [DataRow(new string[0])]
        [DataRow(new string[2]{"a","b"})]
        public void Throw_When_ParamsNumberIncorrect(string[]arrParams)
        {
            // Arrange
            List<string> commandParams = new List<string>();
            commandParams.AddRange(arrParams);
            var cmd = new CreateMemberCommand(commandParams, new FakeDb(), new FakeWriter());

            // Act & Assert
            Assert.ThrowsException<ArgumentException>(() => cmd.Execute());
        }

    }
}
