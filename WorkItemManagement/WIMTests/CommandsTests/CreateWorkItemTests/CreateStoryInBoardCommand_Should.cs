﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Commands.CreateWorkItem;
using WorkItemManagement.Commands.CreateWorkItemInBoard;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using static WIMTests.CommandsTests.CreateNewTeamCommand_Should;

namespace WIMTests.CommandsTests.CreateWorkItemTests
{
    [TestClass]
    public class CreateStoryInBoardCommand_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
        public void ThrowWhen_ParamsNumberInvalid()
        {
            //Arrange 
            List<string> commandParams = new List<string>() { "teamName", "boardName", "lastButNotEnough" };

            //Act & Assert
            new CreateStoryInBoardCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Team with such name does not exists")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "teamName",
                "boardName",
                "storyTitle",
                "storyDescription",
                "High",
                "Small"
            };

            //Act & Assert
            new CreateStoryInBoardCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Board with this name does not exists in this team")]
        public void ThrowWhen_BoardDoesNotExist()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "storyTitle",
                "storyDescription",
                "High",
                "Small"
            };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());

            //Assert
            new CreateStoryInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Story with such title already exists in this team board")]
        public void ThrowWhen_StoryTitleExists()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "storyDescription",
                "High",
                "Small"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeTeam.AddBoard(fakeBoard);
            fakeDb.AddTeam(fakeTeam);
            fakeBoard.AddWorkItem(new FakeWorkItem());

            //Assert
            new CreateStoryInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Story priority should be: high, medium or low")]
        public void ThrowWhen_StoryPriorityInvalid()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "storyTitle",
                "storyDescription",
                "invalidPriority",
                "Small"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            fakeTeam.AddBoard(new FakeBoard());
            fakeDb.AddTeam(fakeTeam);

            //Assert
            new CreateStoryInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Story size should be: small, medium or large")]
        public void ThrowWhen_StorySizeInvalid()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "storyDescription",
                "High",
                "invalidSize"
            };

            //Act

            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            fakeTeam.AddBoard(new FakeBoard());
            fakeDb.AddTeam(fakeTeam);

            //Assert
            new CreateStoryInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }
    }
}
