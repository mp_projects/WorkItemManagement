﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Commands.CreateWorkItemInBoard;

namespace WIMTests.CommandsTests.CreateWorkItemTests
{
    [TestClass]
    public class CreateBugInBoardCommand_Should
    {
        [TestMethod]
        public void ThrowWhen_ParamsNumberInvalid()
        {
            //Arrange 
            List<string> commandParams = new List<string>() { "teamName", "boardName", "lastButNotEnough" };

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new CreateBugInBoardCommand(commandParams, new FakeDb(), new FakeWriter()).Execute());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Team with such name does not exists")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "teamName",
                "boardName",
                "bugTitle",
                "bugDescription",
                "bugPriority",
                "bugSeverity"
            };

            //Act & Assert
            new CreateBugInBoardCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Board with this name does not exists in this team")]
        public void ThrowWhen_BoardDoesNotExist()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "boardName",
                "bugTitle",
                "bugDescription",
                "bugPriority",
                "bugSeverity"
            };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());
            

            //Assert
            new CreateBugInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Bug with such title already exists in this team board")]
        public void ThrowWhen_BugTitleExists()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "bugDescription",
                "High",
                "Critical"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeTeam.AddBoard(fakeBoard);
            fakeDb.AddTeam(fakeTeam);
            fakeBoard.AddWorkItem(new FakeWorkItem());

            //Assert
            new CreateBugInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Bug priority should be: high, medium or low")]
        public void ThrowWhen_BugPriorityInvalid()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "bugTitle",
                "bugDescription",
                "InvalidPriority",
                "Critical"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeTeam.AddBoard(fakeBoard);
            fakeDb.AddTeam(fakeTeam);

            //Assert
            new CreateBugInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Bug severity should be: minor, major or critical")]
        public void ThrowWhen_BugSeverityInvalid()
        {
            //Arrange 
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "bugTitle",
                "bugDescription",
                "High",
                "invalidSeverity"
            };

            //Act
            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeTeam.AddBoard(fakeBoard);
            fakeDb.AddTeam(fakeTeam);

            //Assert
            new CreateBugInBoardCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }
    }
}
