﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands.ChangeStoryCommands;

namespace WIMTests.CommandsTests.ChangeStoryLabelsTests
{
    [TestClass]
    public class ChangeStoryStatusCommand_Should
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
        public void ThrowWhen_InvalidParametersNumber()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "lastButNotEnough"
            };

            new ChangeStoryStatusCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Team does not exists")]
        public void ThrowWhen_TeamDoesNotExist()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "newStatus",
            };

            new ChangeStoryStatusCommand(commandParams, new FakeDb(), new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Board does not exists")]
        public void ThrowWhen_BoardDoesNotExists()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "newStatus",
            };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());

            new ChangeStoryStatusCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Story with such title does not exists")]
        public void ThrowWhen_StoryTitleDoesNotExists()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "newStatus",
            };

            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            fakeTeam.AddBoard(new FakeBoard());
            fakeDb.AddTeam(new FakeTeam());

            new ChangeStoryStatusCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Invalid story size")]
        public void ThrowWhen_StorySizeIsInvalid()
        {
            List<string> commandParams = new List<string>()
            {
                "testTeam",
                "testBoard",
                "testTitle",
                "invalidStatus",
            };

            var fakeDb = new FakeDb();
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard();
            fakeBoard.AddWorkItem(new FakeWorkItem());
            fakeTeam.AddBoard(new FakeBoard());
            fakeDb.AddTeam(new FakeTeam());

            new ChangeStoryStatusCommand(commandParams, fakeDb, new FakeWriter()).Execute();
        }
    }
}
