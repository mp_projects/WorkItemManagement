﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Commands.Abstracts;
using WorkItemManagement.Commands.Contracts;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.CommandsTests
{
    [TestClass]
    public class CreateNewTeamCommand_Should
    {
        [TestMethod]
        public void ThrowWhen_TeamAlreadyExist()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam" };

            //Act
            var fakeDb = new FakeDb();
            fakeDb.AddTeam(new FakeTeam());
            
            //Assert
            Assert.ThrowsException<ArgumentException>(() => new CreateTeamCommand(commandParams, fakeDb, new FakeWriter()).Execute());
        }
        public void ThrowWhen_ParamsNumberIncorrect()
        {
            //Arrange
            List<string> commandParams = new List<string> { "testTeam", "anotherTestTeam" };

            //Act && Assert
            Assert.ThrowsException<ArgumentException>(() => new CreateTeamCommand(commandParams, new FakeDb(), new FakeWriter()).Execute());
        }
    }
}
