﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands.AssignUnassignWorkItem;

namespace WIMTests.CommandsTests.AssignUnassignWorkItemTests
{
    [TestClass]
    public class UnassignWorkItemCommand_Should
    {
        public class AssignWorkItemCommand_Should
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentException), "Invalid parameters number")]
            public void ThrowWhen_ParamsNumberInvalid()
            {
                //Arrange 
                List<string> commandParams = new List<string>() { "teamName", "boardName", "lastButNotEnough" };

                //Act & Assert
                new UnassignWorkItemCommand(commandParams, new FakeDb(), new FakeWriter());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException), "Team with such name does not exists")]
            public void ThrowWhen_TeamDoesNotExist()
            {
                //Arrange 
                List<string> commandParams = new List<string>()
            {
                "testAssignee",
                "testTeam",
                "testBoard",
                "testWorkItemType",
                "testWorkItemTitle"
            };

                //Act & Assert
                new UnassignWorkItemCommand(commandParams, new FakeDb(), new FakeWriter());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException), "Such assignee does not exists in this team")]
            public void ThrowWhen_AssigneeDoesNotExist()
            {
                //Arrange 
                List<string> commandParams = new List<string>()
            {
                "testAssignee",
                "testTeam",
                "testBoard",
                "testWorkItemType",
                "testWorkItemTitle"
            };

                var fakeDb = new FakeDb();
                var fakeTeam = new FakeTeam();
                fakeTeam.AddBoard(new FakeBoard());
                fakeDb.AddTeam(fakeTeam);

                //Act & Assert
                new UnassignWorkItemCommand(commandParams, fakeDb, new FakeWriter());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void ThrowWhen_WrongWorkItem()
            {
                //Arrange 
                List<string> commandParams = new List<string>()
            {
                "testAssignee",
                "testTeam",
                "testBoard",
                "testWorkItemType",
                "testWorkItemTitle"
            };

                //Act
                var fakeDb = new FakeDb();
                var fakeTeam = new FakeTeam();
                fakeTeam.AddBoard(new FakeBoard());
                fakeDb.AddTeam(fakeTeam);
                fakeTeam.AddTeammate(new FakeMember());

                //Assert
                new UnassignWorkItemCommand(commandParams, fakeDb, new FakeWriter());
            }
        }
    }
}
