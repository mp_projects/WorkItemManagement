﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using WorkItemManagement.Core.Contracts;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using static WIMTests.CommandsTests.CreateNewTeamCommand_Should;

namespace WIMTests.CommandsTests
{
    [TestClass]
    public class ShowTeamsActivityCommand_Should
    {
        [TestMethod]
        public void Throw_When_ParamsNumberIncorrect()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testParam", "anotherTestParam" };

            //Act && Assert
            Assert.ThrowsException<ArgumentException>(() => new ShowTeamsActivityCommand(commandParams, new FakeDb(), new FakeWriter()).Execute());
        }

        [TestMethod]
        public void Throw_When_TeamDoesNotExist()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testTeam" };

            //Act && Assert
            Assert.ThrowsException<ArgumentException>(() => new ShowTeamsActivityCommand(commandParams, new FakeDb(), new FakeWriter()).Execute());
        }
    }
}
