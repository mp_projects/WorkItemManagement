﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Commands;
using static WIMTests.CommandsTests.CreateNewTeamCommand_Should;

namespace WIMTests.CommandsTests
{
    [TestClass]
    public class ShowAllMembersCommand_Should
    {
        [TestMethod]
        public void Throw_When_ParamsNumberIncorrect()
        {
            //Arrange
            List<string> commandParams = new List<string>() { "testParam" };

            //Act && Assert
            Assert.ThrowsException<ArgumentException>(() => new ShowAllMembersCommand(commandParams, new FakeDb(), new FakeWriter()).Execute());
        }
    }
}
