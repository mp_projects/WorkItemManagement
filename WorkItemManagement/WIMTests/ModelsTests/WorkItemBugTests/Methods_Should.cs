﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WIMTests.ModelsTests.WorkItemBugTests
{
    [TestClass]
    public class Methods_Should
    {
        [TestMethod]
        public void AddAssignee_Should()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);
            
            int assigneesCountBefore = testBug.Assignees.Count;
            
            //Act
            testBug.AddAssignee(new FakeMember());
            int assigneesCountAfter = testBug.Assignees.Count;

            //Assert
            Assert.AreEqual(assigneesCountBefore+1, assigneesCountAfter);
        }

        [TestMethod]
        public void RemoveAssignee_Should()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);
            
            var fakeMember = new FakeMember();
            
            testBug.AddAssignee(fakeMember);
            
            int assigneesCountBefore = testBug.Assignees.Count;

            //Act
            testBug.RemoveAssignee(fakeMember);
            int assigneesCountAfter = testBug.Assignees.Count;

            //Assert
            Assert.AreEqual(assigneesCountBefore-1, assigneesCountAfter);
        }

        [TestMethod]
        [DataRow(BugStatus.Active)]
        [DataRow(BugStatus.Fixed)]
        public void ChangeStatus_Should(BugStatus newStatus)
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Act
            testBug.ChangeStatus(newStatus);

            //Assert
            Assert.AreEqual(newStatus, testBug.Status);
        }

        [TestMethod]
        [DataRow(BugSeverity.Critical)]
        [DataRow(BugSeverity.Major)]
        [DataRow(BugSeverity.Minor)]
        public void ChangeSeverity_Should(BugSeverity newSeverity)
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Act
            testBug.ChangeSeverity(newSeverity);

            //Assert
            Assert.AreEqual(newSeverity, testBug.Severity);
        }

        [TestMethod]
        [DataRow (Priority.High)]
        [DataRow (Priority.Medium)]
        [DataRow (Priority.Low)]
        public void ChangePriority_Should(Priority newPriority)
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            BugSeverity severity = BugSeverity.Major;
            List<string> stepsToReproduce = new List<string> { };

            Bug testBug = new Bug(title, description, priority, severity, stepsToReproduce);

            //Act
            testBug.ChangePriority(newPriority);

            //Assert
            Assert.AreEqual(newPriority, testBug.Priority);
        }


        private class FakeMember : IMember
        {
            public int ID => throw new NotImplementedException();

            public string Name { get; } = "member name";

            public IReadOnlyList<ITeam> Teams => throw new NotImplementedException();

            public IEventLog EventLog => throw new NotImplementedException();

            public IReadOnlyList<IWorkItem> WorkItems => throw new NotImplementedException();

            public void AddTeam(Team team)
            {
                throw new NotImplementedException();
            }

            public void AssignItem(IBugStory item)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetAllBugs()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetAllStories()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status)
            {
                throw new NotImplementedException();
            }

            public void UnassignItem(IBugStory item)
            {
                throw new NotImplementedException();
            }
        }

    }
}
