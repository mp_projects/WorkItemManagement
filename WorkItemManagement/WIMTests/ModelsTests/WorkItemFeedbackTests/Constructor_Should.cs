﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.WorkItems;

namespace WIMTests.ModelsTests.WorkItemFeedbackTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void AssignCorrectTitle()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";

            //Act
            var testFeedback = new Feedback(title, description);

            //Assert
            Assert.AreEqual(title, testFeedback.Title);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("tit")]
        [DataRow("A successor to the programming language B, C was originally developed ...")]
        public void ThrowExceptionWhenInvalidTitle(string title)
        {
            //Arrange
            //string title = "this is a normal title";
            string description = "new description";

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Feedback(title, description));
        }

        [TestMethod]
        public void AssignCorrectDescription()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";

            //Act
            var testFeedback = new Feedback(title, description);

            //Assert
            Assert.AreEqual(description, testFeedback.Description);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("descr")]
        [DataRow("A successor to the programming language B, C was originally developed at Bell Labs by " +
                    "Dennis Ritchie between 1972 and 1973 to construct utilities running on Unix. It was applied to " +
                    "re-implementing the kernel of the Unix operating system.[6] During the 1980s, C gradually gained popularity." +
                    " It has become one of the most widely used programming languages,[7][8] with C compilers from various vendors" +
                    " available for the majority of existing computer architectures and operating systems. C has been standardized by" +
                    " the ANSI since 1989 (ANSI C) and by the International Organization for Standardization (ISO).")]
        public void ThrowExceptionWhenInvalidDescription(string description)
        {
            //Arrange
            string title = "this is a normal title";
            //string description = "new description";

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Feedback(title, description));
        }

        [TestMethod]
        public void AssignCorrecInitialStatus()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";

            //Act
            var testFeedback = new Feedback(title, description);

            //Assert
            Assert.AreEqual(FeedbackStatus.New, testFeedback.Status);
        }

        [TestMethod]
        public void AssignCorrecInitialRating()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";

            //Act
            var testFeedback = new Feedback(title, description);

            //Assert
            Assert.AreEqual(0, testFeedback.Rating);
        }

        [TestMethod]
        public void EventLogWriteAtConstruction()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";

            //Act
            var testFeedback = new Feedback(title, description);

            //Assert
            Assert.AreEqual(1, testFeedback.EventLog.History.Count);
        }

        [TestMethod]
        public void InitializeCorrectCommentsListType()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "this is a normal description";

            //Act
            var testFeedback = new Feedback(title, description);

            //Assert
            Assert.IsInstanceOfType(testFeedback.Comments, typeof(List<Comment>));
        }

    }
}
