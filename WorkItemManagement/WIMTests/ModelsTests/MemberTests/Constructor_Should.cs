﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;

namespace WIMTests.ModelsTests.MemberTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void SetCorrectMemberName()
        {
            //Arrange
            string name = "Billy";

            //Act
            Member member = new Member(name);

            //Assert
            Assert.AreEqual(name, member.Name);
        }

        public void Throw_When_NameIsNull()
        {
            //Arrange
            string name = null;

            //Act && Assert
            Assert.ThrowsException<ArgumentException>(() => new Member(name));
        }

        public void Throw_When_NameIsOverMaxLength()
        {
            //Arrange 
            string name = new string('a', 16);

            Assert.ThrowsException<ArgumentException>(() => new Member(name));
        }

        public void Throw_When_NameIsUnderMaxLength()
        {
            //Arrange 
            string name = new string('a', 4);

            Assert.ThrowsException<ArgumentException>(() => new Member(name));
        }
    }
}
