﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;
using WorkItemManagement.Models.WorkItems;

namespace WIMTests.ModelsTests.WorkItemStoryTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void AssignCorrectTitle()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.AreEqual(title, testStory.Title);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("tit")]
        [DataRow("A successor to the programming language B, C was originally developed ...")]
        public void ThrowExceptionWhenInvalidTitle(string title)
        {
            //Arrange
            //string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Story(title, description, priority, size));
        }

        [TestMethod]
        public void AssignCorrectDescription()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.AreEqual(description, testStory.Description);
        }

        [TestMethod]
        [DataRow(null)]
        [DataRow("descr")]
        [DataRow("A successor to the programming language B, C was originally developed at Bell Labs by " +
                    "Dennis Ritchie between 1972 and 1973 to construct utilities running on Unix. It was applied to " +
                    "re-implementing the kernel of the Unix operating system.[6] During the 1980s, C gradually gained popularity." +
                    " It has become one of the most widely used programming languages,[7][8] with C compilers from various vendors" +
                    " available for the majority of existing computer architectures and operating systems. C has been standardized by" +
                    " the ANSI since 1989 (ANSI C) and by the International Organization for Standardization (ISO).")]
        public void ThrowExceptionWhenInvalidDescription(string description)
        {
            //Arrange
            string title = "this is a normal title";
            //string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act & Assert
            Assert.ThrowsException<ArgumentException>(() => new Story(title, description, priority, size));
        }

        [TestMethod]
        public void AssignCorrecInitialStatus()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.AreEqual(StoryStatus.NotDone, testStory.Status);
        }


        [TestMethod]
        public void AssignCorrectPriority()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.AreEqual(priority, testStory.Priority);
        }

        [TestMethod]
        public void AssignCorrectSize()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.AreEqual(size, testStory.Size);
        }

        [TestMethod]
        public void EventLogWriteAtConstruction()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.AreEqual(1, testStory.EventLog.History.Count);
        }

        [TestMethod]
        public void InitializeCorrectAssigneesListType()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.IsInstanceOfType(testStory.Assignees, typeof(IList<IMember>));
        }

        [TestMethod]
        public void InitializeCorrectCommentsListType()
        {
            //Arrange
            string title = "this is a normal title";
            string description = "new description";
            Priority priority = Priority.High;
            StorySize size = StorySize.Medium;

            //Act
            var testStory = new Story(title, description, priority, size);

            //Assert
            Assert.IsInstanceOfType(testStory.Comments, typeof(IReadOnlyList<Comment>));
        }

    }
}
