﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;

namespace WIMTests.ModelsTests.EventTests
{
    [TestClass]
    public class Constructor_Should
    {
         [TestMethod]
         public void SetCorrectEventMessage()
        {
            //Arrange
            string eventMessage = "Test event message"; 

            //Act
            Event newEvent = new Event(eventMessage);

            //Assert
            Assert.AreEqual(eventMessage, newEvent.EventMessage);
        }

           
    }
}
