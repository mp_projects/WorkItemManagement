﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Models;

namespace WIMTests.ModelsTests.EventTests
{
    [TestClass]
    public class EventMessage_Should
    {
        public void ThrowWhen_IsNull()
        {
            //Arrange
            string eventMessage = null;

            //Act && Assert
            Assert.ThrowsException<ArgumentException>(() => new Event(eventMessage));
        }

        public void ThrowWhen_IsEmpty()
        {
            //Arrange
            string eventMessage = " ";

            //Act && Assert
            Assert.ThrowsException<ArgumentException>(() => new Event(eventMessage));
        }
    }
}
