﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WIMTests.FakeModelsForCommandsTests;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    public class ShowBoards_Should
    {
        [TestMethod]
        public void ReturnCorrectString()
        {
            //Arrange
            var fakeTeam = new FakeTeam();
            var fakeBoard = new FakeBoard("someName");

            //Act
            fakeTeam.AddBoard(fakeBoard);

            //Assert
            Assert.AreEqual(fakeTeam.ShowBoards(), string.Join(Environment.NewLine, fakeTeam.Boards));
        }

        [TestMethod]
        public void ThrowsWhen_NoBoardsAdded()
        {
            var fakeTeam = new FakeTeam();

            Assert.ThrowsException<ArgumentException>(() => fakeTeam.ShowBoards());
        }

        class FakeTeam : ITeam
        {
            private List<IBoard> boards = new List<IBoard>();
            public string Name => throw new NotImplementedException();

            public IReadOnlyList<IMember> Members => throw new NotImplementedException();

            public IReadOnlyList<IBoard> Boards { get => this.boards; }

            public IEventLog EventLog { get; } = new EventLog();

            public int ID => throw new NotImplementedException();

            public void AddBoard(IBoard board)
            {
                if (this.boards.Contains(board))
                {
                    throw new ArgumentException(string.Format(ErrorStrings.DUPLICATE_OBJECT, "Board", board.Name));
                }

                this.boards.Add(board);

                this.EventLog.AddEvent(new Event(string.Format(EventStrings.BOARD_ADDED, board.Name)));
            }

            public void AddTeammate(IMember member)
            {
                throw new NotImplementedException();
            }

            public void CheckBoardExistance(string boardName)
            {
                throw new NotImplementedException();
            }

            public void CheckMemberExistance(string memberName)
            {
                throw new NotImplementedException();
            }

            public IBoard GetBoard(string boardName)
            {
                throw new NotImplementedException();
            }

            public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public IMember GetMember(string memberName)
            {
                throw new NotImplementedException();
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }

            public string ShowBoards()
            {
                if (this.Boards.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.NO_TEAM_BOARDS_YET);
                }

                return string.Join(Environment.NewLine, this.Boards);
            }

            public string ShowMembers()
            {
                throw new NotImplementedException();
            }
        }

        class FakeBoard : IBoard
        {

            public FakeBoard(string name)
            {
                this.Name = name;
            }
            public string Name { get; }

            public EventLog EventLog => throw new NotImplementedException();

            public IReadOnlyList<IWorkItem> WorkItems => throw new NotImplementedException();

            public int ID => throw new NotImplementedException();

            public override string ToString()
            {
                return this.Name;
            }
            public void AddWorkItem(IWorkItem item)
            {
                throw new NotImplementedException();
            }

            public void CheckWorkItemExistance(string type, string title)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetAllBugs()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IFeedback> GetAllFeedbacks()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetAllStories()
            {
                throw new NotImplementedException();
            }

            public IBug GetBugByTitle(string bugTitle)
            {
                throw new NotImplementedException();
            }

            public IFeedback GetFeedbackByTitle(string feedbackTitle)
            {
                throw new NotImplementedException();
            }

            public IStory GetStoryByTitle(string storyTitle)
            {
                throw new NotImplementedException();
            }

            public IWorkItem GetWorkItemByTypeAndTitle(string type, string title)
            {
                throw new NotImplementedException();
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }
        }
    }
}
