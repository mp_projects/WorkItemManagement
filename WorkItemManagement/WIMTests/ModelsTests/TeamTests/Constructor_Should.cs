using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkItemManagement.Models;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void SetCorrectTeamName()
        {
            //Arrange
            string name = "TheInvincible";

            //Act
            Team team = new Team(name);

            //Assert
            Assert.AreEqual(name, team.Name);
        }
    }
}
