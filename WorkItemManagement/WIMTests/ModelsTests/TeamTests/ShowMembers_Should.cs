﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using WorkItemManagement.Core;
using WorkItemManagement.Core.Strings;
using WorkItemManagement.Models;
using WorkItemManagement.Models.Contracts;
using WorkItemManagement.Models.Enums;

namespace WIMTests.ModelsTests.TeamTests
{
    [TestClass]
    class ShowMembers_Should
    {
        [TestMethod]
        public void ReturnCorrectString()
        {
            //Arrange
            string teamName = "testName";
            var team = new FakeTeam(teamName);

            //Act
            team.AddTeammate(new FakeMember("testName"));

            //Assert
            Assert.AreEqual(string.Join(Environment.NewLine, team.Members), team.ShowMembers());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "No members found")]
        public void ThrowWhen_ThereAreNoMembers()
        {
            //Arrange
            string teamName = "testName";
            var team = new FakeTeam(teamName);

            //Act & Assert
            team.ShowMembers();
        }

        public class FakeTeam : ITeam
        {
            private string name;
            private List<IMember> members;
            public FakeTeam(string name)
            {
                this.name = name;
                members = new List<IMember>();
            }
            public string Name => throw new NotImplementedException();

            public IReadOnlyList<IMember> Members { get => this.members; }

            public IReadOnlyList<IBoard> Boards => throw new NotImplementedException();

            public IEventLog EventLog => throw new NotImplementedException();

            public int ID => throw new NotImplementedException();

            public void AddBoard(IBoard board)
            {
                throw new NotImplementedException();
            }

            public void AddTeammate(IMember member)
            {
                if (this.members.Contains(member))
                {
                    throw new ArgumentException(string.Format(ErrorStrings.MEMBER_EXIST, member.Name, this.Name));
                }

                this.members.Add(member);

                this.EventLog.AddEvent(new Event(string.Format(EventStrings.MEMBER_ADDED, member.Name)));
            }

            public void CheckBoardExistance(string boardName)
            {
                throw new NotImplementedException();
            }

            public void CheckMemberExistance(string memberName)
            {
                throw new NotImplementedException();
            }

            public IBoard GetBoard(string boardName)
            {
                throw new NotImplementedException();
            }

            public List<IBug> GetFilteredWorkItemsBy(BugStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IStory> GetFilteredWorkItemsBy(StoryStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public List<IFeedback> GetFilteredWorkItemsBy(FeedbackStatus status, IBoard board = null)
            {
                throw new NotImplementedException();
            }

            public IMember GetMember(string memberName)
            {
                throw new NotImplementedException();
            }

            public string ListAllItems()
            {
                throw new NotImplementedException();
            }

            public string ShowActivity()
            {
                throw new NotImplementedException();
            }

            public string ShowBoards()
            {
                throw new NotImplementedException();
            }

            public string ShowMembers()
            {
                if (this.Members.Count == 0)
                {
                    throw new ArgumentException(ErrorStrings.NO_TEAM_MEMBERS_YET);
                }

                return string.Join(Environment.NewLine, this.Members);
            }
        }

        public class FakeMember : IMember
        {
            private string name;
            public FakeMember(string name)
            {
                this.name = name;
            }

            public override string ToString()
            {
                return this.name;
            }

            public int ID => throw new NotImplementedException();

            public string Name => throw new NotImplementedException();

            public IReadOnlyList<ITeam> Teams => throw new NotImplementedException();

            public IEventLog EventLog => throw new NotImplementedException();

            public IReadOnlyList<IWorkItem> WorkItems => throw new NotImplementedException();

            public void AddTeam(Team team)
            {
                throw new NotImplementedException();
            }

            public void AssignItem(IBugStory item)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetAllBugs()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetAllStories()
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IStory> GetWorkItemsFilteredBy(StoryStatus status)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IBug> GetWorkItemsFilteredBy(BugStatus status)
            {
                throw new NotImplementedException();
            }

            public void UnassignItem(IBugStory item)
            {
                throw new NotImplementedException();
            }
        }
    }
}
