# **WORK ITEM MANAGEMENT SYSTEM**


## **DESCRIPTION**

### **System description**

**General**\
“Worк Item Management System” is a console application for managing the tasks in a work environment. The application supports a single Database containing all the registered members, created teams and work items. 

Members can be arranged in teams. Each member can participate in multiple teams. Each team can have multiple boards. Each board can contain multiple work items.

**The Members**\
A member has name (5 to 15 symbols), list of assigned work items and activity history. The name must be unique in the whole system (database). Every inclusion of the member in a team, assigning/unassigning of a work item to him or making a comment to a work item by the member is recorded in his activity history. 

**The Teams**\
A team has name (2 to 20 symbols), list of members, list of boards and history. The name must be unique in the whole system (database). After the team creation members could be added to it and team boards could be created. Every inclusion of a member to the team or creation of a team board is recorded in its history. 

**The Boards**\
A board has name (5 to 10 symbols), list of work items and history. The name may not be unique in the system since a board belongs to a concrete team. However, it must be unique in the team boards list. After the board creation work items could be added to it. Every creation of a work item, changing the labels of a work item or making a comment to a work item is recorded in the board’s history.

**The Work Items**\
Work items are of three types – bugs, stories and feedbacks. Each work item has:
-	ID – whole positive number;
-	title (10 to 50 symbols);
-	Description (10 to 500 symbols);
-	list of comments;
-	history.

The ID is a unique number and is generated automatically with the creation of the item. The title may not be unique in the system since an item belongs to a concrete board. However, it must be unique in the board items list. The description is free text. The list of comments contains the comments to the item made by the team members.\
Additionally, each work item type has the next stuff:
-	Bug: list of steps to reproduce, list of assignees (team members), label priority (High, Medium, Low), label severity (Critical, Major, Minor), label status ([Active], Fixed);
-	Story: list of assignees (team members), label priority (High, Medium, Low), label size (Large, Medium, Small), label status ([NotDone], InProgress, Done);
-	Feedback: label status ([New], Unscheduled, Scheduled, Done), label rating (a whole number with initial default value [0]).
In the above text the square brackets [] denote the default status applied to a work item at its creation.

Only a member included in the team can be author of the comments to work items in the team boards. A work item can be assigned only to a member belonging to the same team in which the work item exists. Every change in the label of a work item or assigning/unassigning of a member to it is recorded in its history.

### **Functional description**

The user can:
-	add members to the database;
-	create teams of members;
-	create team boards with work items;
-	change the work item labels;
-	add comments to a work item made from a member;
-	assign/unassign work item to a member;
-	obtain lists of members, teams and team boards as well as lists of work items sorted and filtered as described below in the commands description.

The user can do the above operations by commands input in the console window.

The generic command syntax is the command name followed by the required parameters. The command name and the parameters are separated by slash “/”. The command name is a single word while command parameters could consist of separate words.\
**Command input example:** _changebugseverity/team name/board name/bug title/new severity_

Hitting `enter key` moves the cursor to a new row leaving a blank row.

Exit from the app is achieved by typing `exit` in a new row.


## **COMMANDS LIST**

### **Commands for creation**

**Command name:** `createmember`\
**Parameters:** _memeber name_\
**Description:** Adds a member to database unless memebr with the same name already exists.

**Command name:** `createteam`\
**Parameters:** _team name_\
**Description:** Adds a team to database unless team with the same name already exists.

**Command name:** `createboard`\
**Parameters:** _board name, team name_\
**Description:** Creates a board in a team unless board with the same name already exists in the team boars list.

**Command name:** `createbug`\
**Parameters:** _team name, board name, work item title, work item description, priority, severity, steps to reproduce_\
**Description:** Creates workitem "bug" in the board of a team unless bug with the same title already exists. Bug is created with initial status "Active".

**Command name:** `createstory`\
**Parameters:** _team name, board name, work item title, work item description, priority, size_\
**Description:** Creates workitem "story" in the board of a team unless story with the same title already exists. Story is created with initial status "NotDone".

**Command name:** `createfeedback`\
**Parameters:** _team name, board name, work item title, work item description_\
**Description:** Creates workitem "feedback" in the board of a team unless feedback with the same title already exists. Feedback is created with initial status "New" and initial rating 0.

### **Commands for manipulation**

**Command name:** `addmembertoteam`\
**Parameters:** _member name, team name_\
**Description:** Adds a registered member to a team unless it is already added.

**Command name:** `addworkitemcomment`\
**Parameters:** _team name, board name, work item type, work item title, author name, comment text_\
**Description:** Adds a comment to a workitem in a board in a team. Author should be a member included in the Database.

**Command name:** `changebugpriority`\
**Parameters:** _team name, board name, bug name, new priority_\
**Description:** Changes the priority of a concrete bug work item.

**Command name:** `changebugseverity`\
**Parameters:** _team name, board name, bug name, new severity_\
**Description:** Changes the severity of a concrete bug work item.

**Command name:** `changebugstatus`\
**Parameters:** _team name, board name, bug name, new status_\
**Description:** Changes the status of a concrete bug work item.

**Command name:** `changestorypriority`\
**Parameters:** _team name, board name, story title, new priority_\
**Description:** Changes the priority of a concrete story work item.

**Command name:** `changestorysize`\
**Parameters:** _team name, board name, story title, new size_\
**Description:** Changes the size of a concrete story work item.

**Command name:** `changestorystatus`\
**Parameters:** _team name, board name, story title, new status_\
**Description:** Changes the status of a concrete story work item.

**Command name:** `changefeedbackstatus`\
**Parameters:** _team name, board name, feedback title, new status_\
**Description:** Changes the status of a concrete feedback work item.

**Command name:** `changefeedbackrating`\
**Parameters:** _team name, board name, feedback title, new rating_\
**Description:** Changes the rating of a concrete feedback work item.

**Command name:** `assignworkitem`\
**Parameters:** _assignee (memebr), team name, board name, work item type, work item title_\
**Description:** Assigns a work item to a member. Team, board, work item and memebr must be reated before.

**Command name:** `unassignworkitem`\
**Parameters:** _assignee (memebr), team name, board name, work item type, work item title_\
**Description:** Unassigns a work item to a member. Team, board, work item and memebr must be reated before.

### **Commands for output lists**

**Command name:** `showallmembers`\
**Parameters:** \
**Description:** Outputs a list of all memebrs in Database.

**Command name:** `showallteams`\
**Parameters:** \
**Description:** Outputs a list of all teams.

**Command name:** `showallteamboards`\
**Parameters:** team name, board name\
**Description:** Outputs a list of all boards from the current team.

**Command name:** `showallteammembers`\
**Parameters:** team name\
**Description:** Outputs a list of all members from the current team.

**Command name:** `showallteamsactivity`\
**Parameters:** team name\
**Description:** Outputs a list of all team activities.

**Command name:** `showboardactivity`\
**Parameters:** team name, board name\
**Description:** Outputs a list of all board activies.

**Command name:** `showmemberactivity`\
**Parameters:** member name\
**Description:** Outputs a list of all member activies.

**Command name:** `listallworkitems`\
**Parameters:** \
**Description:** Outputs a list of all workitems in Database.

**Command name:** `listallteamworkitems`\
**Parameters:** team name\
**Description:** Outputs a list of all workitems in the current team.

**Command name:** `listallboardworkitems`\
**Parameters:** team name, board name\
**Description:** Outputs a list of all workitems in the current team board.

**Command name:** `filterworkitemsbystatus`\
**Parameters:** work item type, work item status\
**Description:** Outputs a list of all workitems with the current status.

**Command name:** `filterteamworkitemsbystatus`\
**Parameters:** team name and/or board name, work item type and work item status\
**Description:** Outputs a list of all workitems with the concrete status from the team and/or board.

**Command name:** `filterbystatusandassignee`\
**Parameters:** work item type, work item status, assignee name\
**Description:** Outputs a list of all workitems with the current status assigned to the assignee.

**Command name:** `filterbyworkitemtype`\
**Parameters:** work item type\
**Description:** Outputs a list of all workitems from the concrete type.

**Command name:** `sortworkitemsby`\
**Parameters:** sorting key and/or sorting type (asc/desc)\
**Description:** Outputs a list of all workitems sorted by a sorting key and a sorting type.

## **MODELS CLASS DIAGRAM**
https://gitlab.com/mihailtpopov/workitemmanagement/-/raw/fa306798e091bf5b0582bae5fcd14932c68638ba/WorkItemManagement/WIMDiagram_1.png

## **LINKS**
[Visit our Trello board](https://trello.com/b/omcI9kwB/oop-teamwork-project)
